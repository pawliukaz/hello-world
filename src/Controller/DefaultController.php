<?php

namespace App\Controller;

use App\Form\SimpleFormType;
use App\Service\AppServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     * @param AppServiceInterface $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(AppServiceInterface $service)
    {
        $parameters = [
            'ioTime' => round($service->inputOutputTime(true), 4),
            'networkTime' => round($service->networkTime(true), 4),
            'phpTime' => round($service->phpExecutionTime(true), 4),
        ];
        $service->deleteFiles();
        return $this->render(
            'base.html.twig',
            $parameters
        );
    }

    /**
     * @Route("/simple-page", name="simple_page")
     */
    public function simplePageAction()
    {
        return $this->render('base.html.twig');
    }


    /**
     * @Route("/simple-form", name="simple_form")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function simpleFormAction(Request $request)
    {
        $form = $this->createForm(SimpleFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render('simplePage.html.twig', ['name' => $form->get('name')->getData()]);
        }
        return $this->render('simpleForm.html.twig', ['form' => $form->createView()]);
    }
}
